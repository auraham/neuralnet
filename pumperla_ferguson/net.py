import numpy as np

np.random.seed(42)

def sigmoid_double(x):
    return 1.0 / (1.0 + np.exp(-x))

def sigmoid(z):
    return np.vectorize(sigmoid_double)(z)

class Layer:
    """
    Layers are stacked to build a sequential neural network
    """

    def __init__(self):
        self.params = []
        
        self.prev = None    # a layer knows its predecessor
        self.next = None    # and its successor

        self.input_data = None  # each layer can persist data flowing into
        self.output_data = None # and out of it in the forward pass

        self.input_delta = None     # analogously, a layer holds input and
        self.output_delta = None    # output data for the backward pass

    def connect(self, layer):
        """
        This method connects a layer to its direct
        neighbors in the  sequential network
        """
        self.prev = layer
        self.next = self

    def forward(self):
        """
        Each layer implementation has to provide a function
        to feed input data forward
        """
        raise NotImplementedError

    def get_forward_input(self):
        """
        input_data is reserved for the first layer;
        all other get their input from the previous output
        """
        if self.prev is None:
            # this is the first layer
            return self.input_data
        else:
            # this is a intermediate layer
            return self.prev.output_data

class ActivationLayer(Layer):
    """
    This activation layer uses the sigmoid function
    to activate neurons
    """
    def __init__(self, input_dim):
        super(ActivationLayer, self).__init__()

        self.input_dim = input_dim
        self.output_dim = input_dim     # I think this is not an error!
                                        # becase sigmoid(x) does not change the shape of x

    def forward(self):
        data = self.get_forward_input()
        self.output_data = sigmoid(data)

    def describe(self):
        print("%-20s (input: %3d, output: %3d)" % 
            (self.__class__.__name__, 
            self.input_dim, 
            self.output_dim))

class DenseLayer(Layer):

    def __init__(self, input_dim, output_dim):
        # Dense layers have input and output dimensions
        super(DenseLayer, self).__init__()

        self.input_dim = input_dim
        self.output_dim = output_dim

        # revisar el shape para compararlo con el de keras y karpathy
        self.weight = np.random.randn(output_dim, input_dim)
        self.bias = np.random.randn(output_dim, 1)

        # the layer parameters consist of weights and bias terms
        self.params = [self.weight, self.bias]

        # deltas for weights and biases are set to 0
        self.delta_w = np.zeros(self.weight.shape)
        self.delta_b = np.zeros(self.bias.shape)

    def forward(self):
        data = self.get_forward_input()

        # the forward pass of the dense layer is the affine-linear
        # transformation on input data defined by weights and biases
        self.output_data = np.dot(self.weight, data) + self.bias

    def describe(self):
        print("%-20s (input: %3d, output: %3d)" % 
            (self.__class__.__name__, 
            self.input_dim, 
            self.output_dim))

class SequentialNetwork:
    def __init__(self, loss=None):
        print("Initialize Network...")
        self.layers = []
        self.loss = None  # implement this

    def add(self, layer):
        """
        When you add a layer, you connect it to
        its predecessor and let it describe itself
        """
        self.layers.append(layer)
        layer.describe()
        if len(self.layers) > 1:
            self.layers[-1].connect(self.layers[-2])

    def single_forward(self, x):
        # que cosa es x?
        # x es un vector de (784, 1), p. 88
        # y es un vector de (10, 1) en one-hot encoding, p. 88
        self.layers[0].input_data = x
        for layer in self.layers:
            layer.forward()
        return self.layers[-1].output_data


if __name__ == "__main__":
    
    net = SequentialNetwork()

    net.add(DenseLayer(784, 392))
    net.add(ActivationLayer(392))

    net.add(DenseLayer(392, 196))
    net.add(ActivationLayer(196))
    
    net.add(DenseLayer(196, 10))
    net.add(ActivationLayer(10))


    # --

    x = np.random.randn(784, 1)
    net.single_forward(x)

    # 1. cambiar los pesos por los que encuentre el modelo de keras
    # 2. revisar que el forward pass sea el mismo en ambos