{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "international-judgment",
   "metadata": {},
   "source": [
    "# Calling `backward` twice"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "spanish-auditor",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "If you are new to backpropagation (or revese-mode autodiff), [read this first](https://sidsite.com/posts/autodiff/). It is an awesome introduction to the topic. We will discuss the first example explained in that post.\n",
    "\n",
    "Consider this expression:\n",
    "\n",
    "$$\n",
    "f = a (a + b)\n",
    "$$\n",
    "\n",
    "It is a function that depends on two variables, `a` and `b`. Let's express it using Python and Python:\n",
    "\n",
    "```python\n",
    "def f_fn(a, b):\n",
    "    return a*(a+b)\n",
    "```\n",
    "\n",
    "This is the same example described in [this post](https://sidsite.com/posts/autodiff/) for introducing reverse-mode automatic differentiation (or reverse-mode autodiff for short). This technique allows us to compute partial derivatives or large expressions. In this case, we can use that technique for computing the partial derivative of `f` with respect to `a` and with respect to `b`. The author even provides an implementation using plain Python code. I encorage you to read it. In this post, we instead use PyTorch for computing partial derivatuves, that is, the gradient of `f`:\n",
    "\n",
    "$$\n",
    "\\nabla f = \\left[ \\frac{\\partial{f}}{\\partial{a}}, \\frac{\\partial{f}}{\\partial{b}} \\right]\n",
    "$$\n",
    "\n",
    "The following code creates a few PyTorch tensors, namely `a`, `b`, and `f`. Here, `f` stores the result of calling `f_fn(a, b)`. Since we are interested in computing the gradient of `f` with respect to `a` and with respect to `b`, we must tell PyTorch to track those two variables in the comptation graph. That way, PyTorch can keep track of the partial derivatives of `a` and `b`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "multiple-server",
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "\n",
    "def f_fn(a, b):\n",
    "    return a*(a+b)\n",
    "\n",
    "a = torch.tensor(4.0, requires_grad=True)\n",
    "b = torch.tensor(3.0, requires_grad=True)\n",
    "f = f_fn(a, b)                    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "varying-electricity",
   "metadata": {},
   "source": [
    "We can print the result:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "ongoing-funeral",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor(28., grad_fn=<MulBackward0>)"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "chinese-loading",
   "metadata": {},
   "source": [
    "`f` encodes a computation graph like this:\n",
    "\n",
    "![](abcd.png)\n",
    "\n",
    "PyTorch uses such a computation graph to compute the partial derivatives of `f` with respect to its parameters. To do so, we can use `f.backward()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "helpful-circle",
   "metadata": {},
   "outputs": [],
   "source": [
    "f.backward()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "coral-control",
   "metadata": {},
   "source": [
    "At first, it seems that nothing happened. Although, we are ready to get the partial derivatives as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "czech-allen",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor(11.)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a.grad"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "union-accreditation",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor(4.)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "b.grad"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "convinced-question",
   "metadata": {},
   "source": [
    "This is the full code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "featured-image",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "grad[a]: tensor(11.)\n",
      "grad[b]: tensor(4.)\n"
     ]
    }
   ],
   "source": [
    "import torch\n",
    "\n",
    "def f_fn(a, b):\n",
    "    return a*(a+b)\n",
    "\n",
    "a = torch.tensor(4.0, requires_grad=True)\n",
    "b = torch.tensor(3.0, requires_grad=True)\n",
    "f = f_fn(a, b)                  # forward pass\n",
    "f.backward()                    # backward pass\n",
    "\n",
    "print(\"grad[a]:\", a.grad)       # 11\n",
    "print(\"grad[b]:\", b.grad)       # 4"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "finnish-animal",
   "metadata": {},
   "source": [
    "## Accumulating gradients\n",
    "\n",
    "In [Deep Learning with PyTorch](https://www.manning.com/books/deep-learning-with-pytorch), the authors explained that we must not call `backward` twice. Otherwise, the gradients will accumulate (emphasis is mine):\n",
    "\n",
    "> We could have any number of tensors with `requires_grad` set to `True` and any composition of functions. In this case, PyTorch would compute the derivatives of the loss throughout the chain of functions (the computation graph) and **accumulate** their values in the `grad` attribute of those tensors (the leaf nodes of the graph).\n",
    ">\n",
    "> Alert! *Big gotcha ahead*. This is something PyTorch newcomers--and a lot of more experienced folks, too--trip up on regularly. We just wrote **accumulate**, not **store**.\n",
    ">\n",
    "> **WARNING** Calling `backward` will lead derivatives to **accumulate** at leaf nodes. We need to *zero the gradient explicitly* after using it for parameter updates.\n",
    "\n",
    "In other words, if we call `backward` twice, the gradient will be doubled."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "spread-cover",
   "metadata": {},
   "outputs": [
    {
     "ename": "RuntimeError",
     "evalue": "Trying to backward through the graph a second time, but the saved intermediate results have already been freed. Specify retain_graph=True when calling .backward() or autograd.grad() the first time.",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mRuntimeError\u001b[0m                              Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-3-d95a91b3b077>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      8\u001b[0m \u001b[0mf\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mf_fn\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0ma\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mb\u001b[0m\u001b[0;34m)\u001b[0m     \u001b[0;31m# forward pass\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      9\u001b[0m \u001b[0mf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mbackward\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m       \u001b[0;31m# backward pass\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 10\u001b[0;31m \u001b[0mf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mbackward\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m       \u001b[0;31m# backward pass, again\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     11\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     12\u001b[0m \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"grad[a]:\"\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0ma\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mgrad\u001b[0m\u001b[0;34m)\u001b[0m        \u001b[0;31m# 22\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/.virtualenvs/keras/lib/python3.8/site-packages/torch/tensor.py\u001b[0m in \u001b[0;36mbackward\u001b[0;34m(self, gradient, retain_graph, create_graph, inputs)\u001b[0m\n\u001b[1;32m    243\u001b[0m                 \u001b[0mcreate_graph\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mcreate_graph\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    244\u001b[0m                 inputs=inputs)\n\u001b[0;32m--> 245\u001b[0;31m         \u001b[0mtorch\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mautograd\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mbackward\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mgradient\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mretain_graph\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mcreate_graph\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0minputs\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0minputs\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    246\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    247\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0mregister_hook\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mhook\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/.virtualenvs/keras/lib/python3.8/site-packages/torch/autograd/__init__.py\u001b[0m in \u001b[0;36mbackward\u001b[0;34m(tensors, grad_tensors, retain_graph, create_graph, grad_variables, inputs)\u001b[0m\n\u001b[1;32m    143\u001b[0m         \u001b[0mretain_graph\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mcreate_graph\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    144\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 145\u001b[0;31m     Variable._execution_engine.run_backward(\n\u001b[0m\u001b[1;32m    146\u001b[0m         \u001b[0mtensors\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mgrad_tensors_\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mretain_graph\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mcreate_graph\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0minputs\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    147\u001b[0m         allow_unreachable=True, accumulate_grad=True)  # allow_unreachable flag\n",
      "\u001b[0;31mRuntimeError\u001b[0m: Trying to backward through the graph a second time, but the saved intermediate results have already been freed. Specify retain_graph=True when calling .backward() or autograd.grad() the first time."
     ]
    }
   ],
   "source": [
    "import torch\n",
    "\n",
    "def f_fn(a, b):\n",
    "    return a*(a+b)\n",
    "\n",
    "a = torch.tensor(4.0, requires_grad=True)\n",
    "b = torch.tensor(3.0, requires_grad=True)\n",
    "f = f_fn(a, b)     # forward pass\n",
    "f.backward()       # backward pass\n",
    "f.backward()       # backward pass, again\n",
    "\n",
    "print(\"grad[a]:\", a.grad)        # 22\n",
    "print(\"grad[b]:\", b.grad)        # 8"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dietary-season",
   "metadata": {},
   "source": [
    "This is the code after fixing it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "center-terrorist",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "grad[a]: tensor(22.)\n",
      "grad[b]: tensor(8.)\n"
     ]
    }
   ],
   "source": [
    "import torch\n",
    "\n",
    "def f_fn(a, b):\n",
    "    return a*(a+b)\n",
    "\n",
    "a = torch.tensor(4.0, requires_grad=True)\n",
    "b = torch.tensor(3.0, requires_grad=True)\n",
    "f = f_fn(a, b)     # forward pass\n",
    "f.backward(retain_graph=True)    # backward pass (retain_graph=True is needed to call backward again) \n",
    "f.backward()                     # backward pass, again\n",
    "\n",
    "print(\"grad[a]:\", a.grad)        # 22\n",
    "print(\"grad[b]:\", b.grad)        # 8"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "agricultural-blade",
   "metadata": {},
   "source": [
    "As you can see, the value of the gradient changed from `11` to `22`, and `4` to `8`."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
