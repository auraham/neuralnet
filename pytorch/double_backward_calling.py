# double_backward_calling.py
import torch

def f(a, b):
    return a*(a+b)

a = torch.tensor(4.0, requires_grad=True)
b = torch.tensor(3.0, requires_grad=True)
c = f(a, b)                     # forward pass
c.backward()                    # backward pass

print("grad[a]:", a.grad)       # 11
print("grad[b]:", b.grad)       # 4

# -------
# If we call .backward() twice, the gradient is accumulated
# -------

a = torch.tensor(4.0, requires_grad=True)
b = torch.tensor(3.0, requires_grad=True)
c = f(a, b)     # forward pass
c.backward(retain_graph=True)    # backward pass (retain_graph=True is needed to call .backward() again) 
c.backward()                     # backward pass, again

print("grad[a]:", a.grad)       # 22
print("grad[b]:", b.grad)       # 8

