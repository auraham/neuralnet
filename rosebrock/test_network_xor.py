# test_network_xor.py
import numpy as np
from network import NeuralNetwork

if __name__ == "__main__":

    # construct the XOR dataset
    X = np.array([[0, 0], 
                  [0, 1],
                  [1, 0],
                  [1, 1]])
    y = np.array([[0],
                  [1],
                  [1],
                  [0]])
    
    # define our 2-2-1 neural network and train it
    net = NeuralNetwork([2,2,1], seed=42, alpha=0.5)
    net.fit(X, y, epochs=20000)
    
    # now that our network is trained, loop over the XOR data points
    for (x, target) in zip(X, y):
        
        # make a prediction on the data point and display the result
        # to our console
        pred = net.predict(x)[0][0]
        step = 1 if pred > 0.5 else 0
        
        print("[INFO] data={}, ground-truth={}, pred={:.4f}, step={}".format(
                x, target[0], pred, step))
    
