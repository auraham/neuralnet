# Neural Network in Python

This repository contains four implementations of a multi-layer neural network:

- Nielsen from [http://neuralnetworksanddeeplearning.com/](http://neuralnetworksanddeeplearning.com/)
- Rosebrock from [Deep Learning for Computer Vision](https://www.pyimagesearch.com/deep-learning-computer-vision-python-book/)
- Chollet from [Deep Learning with Python](https://www.manning.com/books/deep-learning-with-python)
- Pytorch from [Deep Learning with PyTorch](https://pytorch.org/deep-learning-with-pytorch) and [jhui.github.io](https://jhui.github.io/2018/02/09/PyTorch-neural-networks/)

The aim of this project is to get a better intuition of how a neural network works. To this end, we use a simple classification problem: classify digits from MNIST. The above four models are implemented in Python. Also, the first two models are created from scratch, so we cover a lot of concepts, like weight initialization, activation functions, forward-pass, backward-pass (backpropagation), stochastic gradient descent, etc.



## Goals

- To get a better understanding of how a multi-layer neural network works. If we understand that, we can implement one from scratch.
- To understand how the convolution operation works.
- To interchange hand-made models with framework-based models. Here, I want to take the weights learned by a model created with Keras and plug them into the models of Rosebrock or Nielsen.



## Structure

Each model is stored in a separated directory:

```
neuralnet/
	nielsen
	rosebrock
	keras
	pytorch
	data         # mnist images
	tests        # testing scripts
```



## Requirements

- Python3
- Numpy
- Keras
- Theano 0.7 (only for Nielsen's code)



## TODO

- Create `requirements.txt`



## More Resources

- [Building a Neural Network from Scratch in Python and in Tensorflow](https://beckernick.github.io/neural-network-scratch/)
- [Micrograd: A tiny scalar-valued autograd engine](https://github.com/karpathy/micrograd)
- [cnn-numpy](https://github.com/neuron-whisperer/cnn-numpy)



## Contact

Auraham Camacho `auraham.cg@gmail.com`