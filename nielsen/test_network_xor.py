# test_network.py
import numpy as np
import mnist_loader
import network

def vectorized_result(j):
    """Return a 2-dimensional unit vector with a 1.0 in the jth
    position and zeroes elsewhere.  This is used to convert a value
    (0, 1) into a corresponding desired output from the neural
    network."""
    e = np.zeros((2, 1))
    e[j] = 1.0
    return e


if __name__ == "__main__":
    
    # construct the XOR dataset
    X = np.array([[0, 0], 
                  [0, 1],
                  [1, 0],
                  [1, 1]])
    y = np.array([0,
                  1,
                  1,
                  0])
    
    # reshape dataset
    training_inputs = [np.reshape(x, (2, 1)) for x in X]
    training_results = [vectorized_result(y) for y in y]
    training_data = list(zip(training_inputs, training_results))
    
    # it did not work!
    # train network
    net = network.Network([2, 2, 2])
    net.SGD(training_data, 30, 1, 3.0)#, test_data=test_data)
