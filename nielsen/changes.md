# Changes

- Replace `xrange` with `range`.
- Replace `print str` with `print(str)`.
- Use `pickle` rather than `cPickle` as discussed [here](https://rebeccabilbro.github.io/convert-py2-pickles-to-py3/) and [here](https://stackoverflow.com/questions/49579282/cant-find-module-cpickle-using-python-3-5-and-anaconda):

```python
# mnist_loader.py
import pickle

def load_data():
    f = gzip.open('data/mnist.pkl.gz', 'rb')
    training_data, validation_data, test_data = pickle.load(f, encoding="bytes") # change
    f.close()
    return (training_data, validation_data, test_data)
```

- `len(zip)`. You will get this error (`network.py`, line 54):

```
TypeError: object of type 'zip' has no len()
```

This is because we cannot get the length of a zip object:

```python
# python2
a = [1,2,3]
b = [3,4,5]
c = zip(a,b)   # python2: c = [(1, 3), (2, 4), (3, 5)]
len(c)         # python2: 3
```

Although, the last line will throw an error in Python 3.

To fix this error, we modified `mnist_loader.py` by replacing `zip(iterables)` with `list(zip(iterables))` as recommended in [stackoverflow](https://stackoverflow.com/questions/31011631/python-2-3-object-of-type-zip-has-no-len):

```python
def load_data_wrapper():
	tr_d, va_d, te_d = load_data()
    training_inputs = [np.reshape(x, (784, 1)) for x in tr_d[0]]
    training_results = [vectorized_result(y) for y in tr_d[1]]
    training_data = list(zip(training_inputs, training_results))     # changed
    validation_inputs = [np.reshape(x, (784, 1)) for x in va_d[0]]
    validation_data = list(zip(validation_inputs, va_d[1]))          # changed
    test_inputs = [np.reshape(x, (784, 1)) for x in te_d[0]]
    test_data = list(zip(test_inputs, te_d[1]))                      # changed
    return (training_data, validation_data, test_data)
```





## About the dataset

The dataset is loaded by the `load_data_wrapper` function in `mnist_loader.py`. It splits the dataset into three sets: `training_data`, `validation_data`, and `test_data`:

```python
# python 2
ipdb> len(training_data)
50000
ipdb> len(validation_data)
10000
ipdb> len(test_data)
10000
```

Each entry in each set is a tuple with two Numpy arrays:

```python
ipdb> pp type(training_data[0])
<type 'tuple'>
ipdb> pp type(training_data[0][0])
<type 'numpy.ndarray'>
ipdb> pp type(training_data[0][1])
<type 'numpy.ndarray'>
```

The first array is the image (`shape = []`), whereas the second array contains the label stored using one-hot encoding (`shape = []`):

```python
ipdb> pp training_data[0][0].shape      # input x
(784, 1)
ipdb> pp training_data[0][1].shape      # label y
(10, 1)
```

The first training image is shown below. Notice that the values are in [0, 1], where 0 is black.

```python
ipdb> pp training_data[0][0]
array([[0.        ],
       [0.        ],
       ...
       [0.01171875],
       [0.0703125 ],
       [0.0703125 ],
       [0.0703125 ],
	   ...
	   [0.        ],
       [0.        ]], dtype=float32)
```

Here is the encoded label of the first training image:

```python
ipdb> pp training_data[0][1]
array([[0.],
       [0.],
       [0.],
       [0.],
       [0.],
       [1.],
       [0.],
       [0.],
       [0.],
       [0.]])
```


