# Notes





## Differences

Nielsen usa mini=batch, rosebrock usa 1 example each time.





## Weight Initialization

In `rosebrock/network.py`, the author initializes the weights using Xavier initialization:

```python
def __init__(self, layers, alpha=0.1):
        """Initialize the list of weights matriceds, then store the
        network architecture and learning rate"""
        
        self.W = []
        self.layers = layers
        self.alpha = alpha
        
        # start looping from the index of the first layer but
        # stop before we reach the last two layers
        for i in np.arange(0, len(layers) - 2):
            
            # randomy initialize a weight matrix connecting the 
            # number of nodes in each respective layer together.
            # adding an extra node for the bias
            w = np.random.randn(layers[i] + 1, layers[i+1] + 1)
            
            # Xavier initialization
            w = w / np.sqrt(layers[i])
            
            self.W.append(w)
```

You can find more examples of that initialization heuristic in the [C231n course website](https://cs231n.github.io/neural-networks-2/#init). From that site:

> **Calibrating the variances with 1/sqrt(n)** [...] The recommended heuristic is to initialize each neuron's weight as:
>
> `w = np.random.randn(n) / sqrt(n)`, 
>
> where `n` is the number of its inputs. This ensures that all neurons in the network initially have approximately the same output distribution and empirically improves the rate of convergence.

Also, you can find more code in the repository of [Karpathy](https://gist.github.com/karpathy/a4166c7fe253700972fcbc77e4ea32c5):

```python
# model initialization
H = 200 # number of hidden layer neurons
D = 80 * 80 # input dimensionality: 80x80 grid
if resume:
  model = pickle.load(open('save.p', 'rb'))
else:
  model = {}
  model['W1'] = np.random.randn(H,D) / np.sqrt(D) # "Xavier" initialization
  model['W2'] = np.random.randn(H) / np.sqrt(H)
```

However, you may notice that both implementations differ:

```python
# rosebrock
w = np.random.randn(layers[i] + 1, layers[i+1] + 1) / np.sqrt(layers[i])
```

```python
# karpathy
model['W1'] = np.random.randn(H,D) / np.sqrt(D)
```

At first I thought it was a mistake. Now, I think both approaches are correct. As explained in the  [C231n course website](https://cs231n.github.io/neural-networks-2/#init), the divisor is `np.sqrt(n)`, where `n` is the number of inputs. Rosebrock uses this format:

```python
# rosebrock
np.random.randn(n_inputs + bias, n_outputs + bias) / np.sqrt(n_inputs)
```

whereas Karpathy uses this one:

```python
# karpathy
np.random.randn(n_outputs, n_inputs) / np.sqrt(n_inputs)   # bias is not implemented because meh
```

It turns out it is just a different convention. Both codes are correct.



## Shape of weight matrices

Somewhat related to the previous section, Rosebrock and Karpathy use different shapes for the weight matrices. Before explaining the difference, let us check how to determine the size of a neural network according to [this post](https://cs231n.github.io/neural-networks-1/). Below, there is a 3-layer neural network with three inputs, two hidden layers of 4 neurons each and one output layer.



![](img/neural_net2.jpeg)

This network has:

- 4 + 4 + 1 neurons (or units)
- [3 x 4] + [4 x 4] + [4x1] = 12 + 16 + 4 = 32 weights and
- 4 + 4 + 1 = 9 biases

This is a total of 41 learnable parameters.

The Karpathy's Python code for the previous network is given below:

```python
# forward-pass of a 3-layer neural network:
f   = lambda x: 1.0/(1.0 + np.exp(-x))   # activation function (use sigmoid)
x   = np.random.randn(3, 1)              # random input vector of three numbers (3x1)
h1  = f(np.dot(W1, x) + b1)              # calculate first hidden layer activations (4x1)
h2  = f(np.dot(W2, h1) + b2)             # calculate second hidden layer activations (4x1)
out = np.dot(W3, h2) + b3                # output neuron (1x1)
```

> Working with the example three-layer neural network in the diagram above, the input would be a [3x1] vector. All connection strengths (i.e., weights) for a layer can be stored in a single matrix. For example, the first hidden layer's weights







