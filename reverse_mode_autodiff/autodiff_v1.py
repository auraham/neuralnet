# autodiff_v1.py
# https://sidsite.com/posts/autodiff/

from collections import defaultdict

class Variable:

    def __init__(self, value, local_gradients=(), label=""):
        self.label = label
        self.value = value
        self.local_gradients = local_gradients

    def __repr__(self):
        #grads_str = " ".join("%s" % (i,) for i in self.local_gradients)
        #return "%s: %d, grads: [%s]" % (self.label, self.value, self.local_gradients)
        
        grads = "<" + ", ".join("%s" % (i,) for i in self.local_gradients) + ">"
        return "%s:%d grads [%d]:%s" % (self.label, self.value, len(self.local_gradients), grads)
        
        #return "%s: %d, grads:\n%s" % (self.label, self.value, grads_str)

def add(a, b, label):
    """Create the variable that results from adding two variables"""
    value = a.value + b.value
    local_gradients = (
        (a, 1),
        (b, 1)
    )
    return Variable(value, local_gradients, label)

def mul(a, b, label):
    """Create the variable that results from multiplying two variables"""
    value = a.value * b.value
    local_gradients = (
        (a, b.value),
        (b, a.value)
    )
    return Variable(value, local_gradients, label)

def get_gradients(variable):
    """Compute the first derivaties of `variable`
    with respect to child variables
    """
    gradients = defaultdict(lambda: 0)

    import ipdb; ipdb.set_trace()

    def compute_gradients(variable, path_value):

        for child_variable, local_gradient in variable.local_gradients:
            # Multiply the edges of a path
            value_of_path_to_child = path_value * local_gradient
            # Add together the different paths
            gradients[child_variable] += value_of_path_to_child # ojo, es gradients[child_variable], no gradients[variable]
            # recurse through the graph
            compute_gradients(child_variable, value_of_path_to_child) # CONTINUA AQUI
            
    # path_value=1 is from `variable` differentiated wrt itself
    compute_gradients(variable, path_value=1)

    return gradients


if __name__ == "__main__":

    a = Variable(4, label="a")
    b = Variable(3, label="b")
    c = add(a, b, label="c")
    d = mul(a, c, label="d")

    import ipdb; ipdb.set_trace()

    gradients = get_gradients(d)

    print("d.value =", d.value)
    print("The partial derivative of d wrt a =", gradients[a])
