# linear_regression_pytorch.py
import torch

def model(t_u, w, b):
    return t_u*w + b

def loss_fn(t_p, t_c):
    squared_diffs = (t_p - t_c)**2
    return squared_diffs.mean()

if __name__ == "__main__":

    t_c = [0.5, 14.0, 15.0, 28.0, 11.0, 8.0, 3.0, -4.0, 6.0, 13.0, 21.0]        # ground truth
    t_u = [35.7, 55.9, 58.2, 81.9, 56.3, 48.9, 33.9, 21.8, 48.4, 60.4, 68.4]    # inputs

    t_c = torch.tensor(t_c)
    t_u = torch.tensor(t_u)
    t_un = t_u * 0.1        # scaling for better training

    params = torch.tensor([1.0, 0.0], requires_grad=True)
    lr = 0.00001

    for epoch in range(100):

        if params.grad is not None:     # manually reset gradient to zero
            params.grad.zero_()         # before calling loss.backward()

        t_p = model(t_un, *params)      # forward pass
        loss = loss_fn(t_p, t_c)        # loss(prediction, ground_truth)
        loss.backward()                 # backward pass

        with torch.no_grad():           # temporally sets requires_grad to False
            params = params - lr * params.grad

        print("Loss: %.4f" % loss)