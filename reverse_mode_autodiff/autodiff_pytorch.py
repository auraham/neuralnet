# autodiff_pytorch.py
import torch

def model(t_u, w, b):
    return w * t_u + b

def loss_fn(t_p, t_c):
    # MSE
    # t_p: prediction
    # t_c: groundtruth
    squared_diffs = (t_p - t_c)**2
    return squared_diffs.mean()

t_c = [0.5, 14.0, 15.0, 28.0, 11.0, 8.0, 3.0, -4.0, 6.0, 13.0, 21.0]
t_u = [35.7, 55.9, 58.2, 81.9, 56.3, 48.9, 33.9, 21.8, 48.4, 60.4, 68.4]
t_c = torch.tensor(t_c)
t_u = torch.tensor(t_u)
params = torch.tensor([1.0, 0.0], requires_grad=True)  # [w=1, b=0]

# page 124
loss = loss_fn(model(t_u, *params), t_c)
loss.backward()

params.grad


# otro ejemplo
params = torch.tensor([4.0, 3.0], requires_grad=True)  # a=4, b=3

def f(a, b):
    return a*(a+b)

result = f(*params)
result.backward()

params.grad  # [11, 4]

# otro ejemplo
a = torch.tensor([4.0], requires_grad=True)
b = torch.tensor([3.0], requires_grad=True)
c = a + b
d = a * c
d.backward()

print(a.grad)
print(b.grad)

# otro ejemplo (creo que es mas cercano al ejemplo de sid)
a = torch.tensor(4.0, requires_grad=True)
b = torch.tensor(3.0, requires_grad=True)
c = a + b
d = a * c
d.backward()
print("grad[a] =", a.grad)
print("grad[b] =", b.grad)

# otro ejemplo
def f(a, b, c):
    f = torch.sin(a*b) + torch.exp(c - (a/b))
    return torch.log(f*f) * c

a = torch.tensor(43.0, requires_grad=True, dtype=torch.float64)  # tienes que especificar dtype=torch.float64 si quieres usar diferncias finitas
b = torch.tensor( 3.0, requires_grad=True, dtype=torch.float64)
c = torch.tensor( 2.0, requires_grad=True, dtype=torch.float64)
y = f(a, b, c)

y.backward()

print("grads[a] =", a.grad)  # derivada parcial de f con respecto a a
print("grads[b] =", b.grad)  # derivada parcial de f con respecto a b
print("grads[c] =", c.grad)  # derivada parcial de f con respecto a c

# estimamos el resultado de cada derivada parcial usando diferencias finitas
# ver lecture 6: automatic differentiation de Roger Grosse
delta = 0.0000001
numerical_grad_a = (f(a + delta, b, c) - f(a, b, c)) / delta

numerical_grad_a = (f(a + delta, b, c) - f(a, b, c)) / delta
numerical_grad_b = (f(a, b + delta, c) - f(a, b, c)) / delta
numerical_grad_c = (f(a, b, c + delta) - f(a, b, c)) / delta

print(numerical_grad_a)
print(numerical_grad_b)
print(numerical_grad_c)